const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', responseMiddleware , (req, res, next) => {
  try {
    const users = FighterService.getUsers();
    if(users) {
      res.status(200);
      res.data = users;
    }
  } catch (err) {
    res.status(404);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const userToFind = FighterService.search({ id });
    if (userToFind) {
      res.status(200)
      res.data = userToFind;
    }
    } catch (err) {
    res.status(404);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  try {
    const userToCreate = FighterService.createUser(req.body);
    if (userToCreate) {
      res.status(200)
      res.data = userToCreate;
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    const id = req.params.id;
    const userData = req.body;
    const userToUpdate = FighterService.updateUser(id, userData);
    if (userToUpdate) {
      res.status(200)
      res.data = userToUpdate;
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const userToDelete = FighterService.deleteUser(id);
    if (userToDelete) {
      res.status(200);
      res.data = userToDelete;
    }
  } catch (err) {
    res.status(404);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;

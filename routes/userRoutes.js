const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

// GET /api/users getUsers()
router.get('/', responseMiddleware , (req, res, next) => {
  try {
    const users = UserService.getUsers();
    if(users) {
      res.status(200);
      res.data = users;
    }
  } catch (err) {
    res.status(404);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

// GET /api/users/:id search(search)
router.get('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const userToFind = UserService.search({ id });
    if (userToFind) {
      res.status(200)
      res.data = userToFind;
    }
    } catch (err) {
    res.status(404);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

// POST /api/users createUser(data) createUserValid
router.post('/', createUserValid, (req, res, next) => {
  try {
    const userToCreate = UserService.createUser(req.body);
    if (userToCreate) {
      res.status(200)
      res.data = userToCreate;
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

// PUT /api/users/:id updateUser(id, data) updateUserValid
router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    const id = req.params.id;
    const userData = req.body;
    const userToUpdate = UserService.updateUser(id, userData);
    if (userToUpdate) {
      res.status(200)
      res.data = userToUpdate;
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

// DELETE /api/users/:id deleteUser(id)
router.delete('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const userToDelete = UserService.deleteUser(id);
    if (userToDelete) {
      res.status(200);
      res.data = userToDelete;
    }
  } catch (err) {
    res.status(404);
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;

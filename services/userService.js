const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    // USER:
    // GET /api/users
    // GET /api/users/:id
    // POST /api/users
    // PUT /api/users/:id
    // DELETE /api/users/:id

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getUsers() {
        const users = UserRepository.getAll();
        if(!users) {
            throw Error('No users were found!');
        }
        return users;
    }

    createUser(data) {
        const userToCreate = UserRepository.create(data);
        if (!userToCreate) {
          throw Error("No user were created!");
        }
        return userToCreate;
    }

    updateUser(id, data) {
        const userToUpdate = UserRepository.update(id, data);
        if (!userToUpdate) {
          throw Error(`Can't update user with ${id} id!`);
        }
        return userToUpdate;
    }

    deleteUser(id) {
        const userToDelete = UserRepository.delete(id);
        if (!userToDelete) {
          throw Error(`Can't find user with ${id} id!`);
        }
        return userToDelete;
    }
}

module.exports = new UserService();

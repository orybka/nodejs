const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getUsers() {
        const users = FighterRepository.getAll();
        if(!users) {
            throw Error('No users were found!');
        }
        return users;
    }

    createUser(data) {
        const userToCreate = FighterRepository.create(data);
        if (!userToCreate) {
          throw Error("No user were created!");
        }
        return userToCreate;
    }

    updateUser(id, data) {
        const userToUpdate = FighterRepository.update(id, data);
        if (!userToUpdate) {
          throw Error(`Can't update user with ${id} id!`);
        }
        return userToUpdate;
    }

    deleteUser(id) {
        const userToDelete = FighterRepository.delete(id);
        if (!userToDelete) {
          throw Error(`Can't find user with ${id} id!`);
        }
        return userToDelete;
    }
}

module.exports = new FighterService();

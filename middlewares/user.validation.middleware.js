const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        validate(req);
    } catch (err) {
        res.status(400);
        res.err = err;
    } finally {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        validate(req);
    } catch (err) {
        res.status(400);
        res.err = err;
    } finally {
        next();
    }
}

const validate = (req) => {
    const { body } = req;

    if(body.hasOwnProperty('id')){
        throw Error(`Request body must not contain id!`);
    }

    if (isEmpty(body.firstName)) {
        console.log(user);
        throw Error('First name must not be empty!');
    } else if (!validateName(body.firstName)) {
        throw Error('First name must contain only letters!');
    }

    if (isEmpty(body.lastName)) {
        throw Error('Last name must not be empty!');
    } else if (!validateName(body.lastName)) {
        throw Error('Last name must contain only letters!');
    }

    if (isEmpty(body.email)) {
        throw Error('Email must not be empty!');
    } else if (!validateEmail(body.email)) {
        throw Error('Email must be name@gmail.com, where name has at least 3 characters!');
    }

    if (isEmpty(body.phoneNumber)) {
        throw Error('Phone number must not be empty!');
    } else if (!validatePhone(body.phoneNumber)) {
        throw Error('Phone number must be +380xxxxxxxxx format!');
    }

    if (isEmpty(body.password)) {
        throw Error('Password must not be empty!');
    } else if (!validatePassword(body.password)) {
        throw Error('Password must contain at least 1 number, 1 lowercase and 1 uppercase letter!');
    }
};

const isEmpty = (item) => {
    return item === '' || !item || item.length === 0;
};

const validateName = (name) => {
    const regName = /^[a-zA-Z\s]*$/;
    return regName.test(name);
};

const validateEmail = (email) => {
    const regEmail = /^[a-z0-9](\.?[a-z0-9]){3,}@gmail\.com$/;
    return regEmail.test(email);
};

const validatePhone = (num) => {
    const regNum = /^\+380\d{9}$/;
    return regNum.test(String(num));
};

const validatePassword = (pass) => {
    const regPass = /^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*)$/;
    return regPass.test(pass);
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

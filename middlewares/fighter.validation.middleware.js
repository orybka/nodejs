const { fighter } = require('../models/fighter');

// "id": "",
// "name": "",
// "health": 100,
// "power": 0,
// "defense": 1, // 1 to 10

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        validate(req);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        validate(req);
    } catch (err) {
        res.status(400);
        res.err = err;
    } finally {
        next();
    }
}

const validate = (req) => {
    const { body } = req;

    if(body.hasOwnProperty('id')){
        throw Error(`Request body must not contain id!`);
    }

    if (isEmpty(body.name)) {
        throw Error('Name must not be empty!');
    } else if (!validateName(body.firstName)) {
        throw Error('Name must contain only letters!');
    }

    if (isEmpty(body.health)) {
        body.health = 100;
    } else if (!validateHealth(body.health)) {
        throw Error('Health must contain only numbers and be more than 1 less than 120!');
    }

    if (isEmpty(body.power)) {
        throw Error('Power must not be empty!');
    } else if (!validatePower(body.power)) {
        throw Error('Power must contain only numbers and be more than 0!');
    }

    if (isEmpty(body.defense)) {
        throw Error('Defense number must not be empty!');
    } else if (!validateDefense(body.defense)) {
        throw Error('Defense must contain only numbers and be more than 1 less than 100!');
    }
};

const isEmpty = (item) => {
    return item === '' || !item || item.length === 0;
};

const validateName = (name) => {
    const regName = /^[a-zA-Z\s]*$/;
    return regName.test(name);
};

const validatePower = (power) => {
    if(typeof power == "number" && Number.isInteger(power) && power >= 1 && power <= 100) {
        return true;
    }
};

const validateDefense = (defence) => {
    if(typeof defence == "number" && Number.isInteger(defence) && defence >= 1 && defence <= 10) {
        return true;
    }
};

const validateHealth = (health) => {
    if(typeof health == "number" && Number.isInteger(health) && health >= 80 && health <= 120) {
        return true;
    }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
